import express from 'express'
import {
    getRekapitulasi, getRekapitulasiByKecamatan,
    getRekapitulasiRetribusiByKecamatan, getRekapitulasiSKHPByKecamatan, getRekapitulasiUTTPByKecamatan
} from '../../controllers/Pelaporan/PelaporanController.js';

const router = express.Router()

router.get('/:tahun', getRekapitulasi)
router.get('/kecamatan/:tahun', getRekapitulasiByKecamatan)
router.get('/uttp/:tahun', getRekapitulasiUTTPByKecamatan)
router.get('/skhp/:tahun', getRekapitulasiSKHPByKecamatan)
router.get('/retribusi/:tahun', getRekapitulasiRetribusiByKecamatan)

export default router;