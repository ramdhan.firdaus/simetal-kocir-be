import express from 'express'
import { getTargetRetribusi, getTargetRetribusiByTahun, createTargetRetribusi } from '../../controllers/Pelaporan/TargetRetribusiController.js';

const router = express.Router()

router.get('/', getTargetRetribusi)
router.get('/:tahun', getTargetRetribusiByTahun)
router.post('/', createTargetRetribusi)

export default router;