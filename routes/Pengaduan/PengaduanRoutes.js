import express from 'express'
import { getPengaduan, getPengaduanByID, createPengaduan, updatePengaduan } from '../../controllers/Pengaduan/PengaduanController.js';

const router = express.Router()

router.get('/', getPengaduan)
router.get('/:id', getPengaduanByID)
router.post('/', createPengaduan)
router.put('/:id', updatePengaduan)

export default router;