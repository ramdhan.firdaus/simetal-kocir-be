import express from 'express'
import { getBuktiPengaduan, getPengaduanByIDPengaduan, createBuktiPengaduan } from '../../controllers/Pengaduan/BuktiPengaduanController.js';
import { upload } from '../../helpers/filehelper.js'

const router = express.Router()

router.get('/', getBuktiPengaduan)
router.get('/:id', getPengaduanByIDPengaduan)
router.post('/:id', upload.array('files'), createBuktiPengaduan)

export default router;