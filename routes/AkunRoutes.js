import express from 'express'
import { getAkun, RegisterAkun, getAkunPetugas, getAllAkun, updateAkun } from '../controllers/AkunController.js'
import { verifyToken } from "../middleware/VerifyToken.js";

const router = express.Router()

router.get('/', verifyToken, getAkun)
router.get('/all', getAllAkun)
router.post('/', RegisterAkun) 
router.get('/petugas', getAkunPetugas)
router.put('/', updateAkun)

export default router;