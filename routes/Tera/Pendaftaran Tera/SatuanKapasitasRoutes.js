import express from 'express'
import { getSatuanKapasitas } from '../../../controllers/Tera/Pendaftaran Tera/SatuanKapasitasController.js'

const router = express.Router()

router.get('/', getSatuanKapasitas)

export default router;