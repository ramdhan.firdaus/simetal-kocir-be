import express from 'express'
import { getPendaftaranTera, getPendaftaranTeraNotSPT, getPendaftaranTeraByNo, createPendaftaran, updatePendaftaran, getPendaftaranTeraNotPengamatan, getPendaftaranTeraNotPengawasan }
    from '../../../controllers/Tera/Pendaftaran Tera/PendaftaranTeraController.js'

const router = express.Router()

router.get('/', getPendaftaranTera)
router.get('/not-spt', getPendaftaranTeraNotSPT)
router.get('/not-pengawasan', getPendaftaranTeraNotPengawasan)
router.get('/not-pengamatan', getPendaftaranTeraNotPengamatan)
router.get('/:no', getPendaftaranTeraByNo)
router.post('/', createPendaftaran)
router.put('/:no', updatePendaftaran)

export default router;