import express from 'express'
import { getJenisUTTP } from '../../../controllers/Tera/Pendaftaran Tera/JenisUTTPSubController.js'

const router = express.Router()

router.get('/', getJenisUTTP)

export default router;