import express from 'express'
import { getJenisUTTP } from '../../../controllers/Tera/Pendaftaran Tera/JenisUTTPController.js'

const router = express.Router()

router.get('/', getJenisUTTP)

export default router;