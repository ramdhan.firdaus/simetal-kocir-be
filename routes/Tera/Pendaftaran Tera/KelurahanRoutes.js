import express from 'express'
import { getKelurahan, getKelurahanByIdKecamatan } from '../../../controllers/Tera/Pendaftaran Tera/KelurahanController.js'

const router = express.Router()

router.get('/', getKelurahan)
router.get('/:kecamatan', getKelurahanByIdKecamatan)

export default router;