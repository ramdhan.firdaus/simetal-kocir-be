import express from 'express'
import { getUTTP, getUTTPById, getUTTPByTahun, createUTTP, updateUTTP } from '../../../controllers/Tera/Pendaftaran Tera/UTTPController.js'

const router = express.Router()

router.get('/', getUTTP)
router.get('/:id', getUTTPById)
router.get('/tahun/:tahun', getUTTPByTahun)
router.post('/', createUTTP)
router.put('/:id', updateUTTP)

export default router;