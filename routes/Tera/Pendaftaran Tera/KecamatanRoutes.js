import express from 'express'
import { getKecamatan } from '../../../controllers/Tera/Pendaftaran Tera/KecamatanController.js'

const router = express.Router()

router.get('/', getKecamatan)

export default router;