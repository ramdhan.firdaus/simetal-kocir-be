import express from 'express'
import { getPelayananTera, getPelayananTeraUji, getPelayananTeraUjiBayar, createPelayananTera, updatePelayananTera, bayarPelayananTera } from '../../../controllers/Tera//Pelayanan Tera/PelayananTeraController.js'

const router = express.Router()

router.get('/', getPelayananTera)
router.get('/uji', getPelayananTeraUji)
router.get('/bayar', getPelayananTeraUjiBayar)
router.post('/', createPelayananTera)
router.put('/:id', updatePelayananTera)
router.put('/bayar/:id', bayarPelayananTera)

export default router;