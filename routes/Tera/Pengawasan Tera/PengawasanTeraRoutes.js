import express from 'express'
import { getPengawasanTera, getPengawasanTeraById, createPengawasanTera } from '../../../controllers/Tera/Pengawasan Tera/PengawasanTeraController.js'

const router = express.Router()

router.get('/', getPengawasanTera)
router.get('/:id', getPengawasanTeraById)
router.post('/', createPengawasanTera)

export default router;