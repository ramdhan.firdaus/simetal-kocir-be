import express from 'express'
import { getPengamatanTera, getPengamatanTeraById, createPengamatanTera } from '../../../controllers/Tera/Pengamatan Tera/PengamatanTeraController.js'

const router = express.Router()

router.get('/', getPengamatanTera)
router.get('/:id', getPengamatanTeraById)
router.post('/', createPengamatanTera)

export default router;