import express from 'express'
import { getPetugasSPT, getPetugasSPTByNoOrder, createPetugasSPT, updatePetugasSPT, deletePetugasSPT } from '../../controllers/SPT/PetugasSPTController.js';

const router = express.Router()

router.get('/', getPetugasSPT)
router.get('/:no', getPetugasSPTByNoOrder)
router.post('/:no', createPetugasSPT)
router.put('/', updatePetugasSPT)
router.delete('/', deletePetugasSPT)

export default router;