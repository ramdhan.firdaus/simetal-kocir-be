import express from 'express'
import { getRevisi, getRevisiByNoOrder, createRevisi, updateRevisi } from '../../controllers/SPT/RevisiSPTController.js'

const router = express.Router()

router.get('/', getRevisi)
router.get('/:no', getRevisiByNoOrder)
router.post('/', createRevisi)
router.put('/:id', updateRevisi)

export default router;