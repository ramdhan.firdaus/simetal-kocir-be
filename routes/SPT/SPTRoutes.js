import express from 'express'
import { getSPT, getSPTByNoOrder, createSPT, updateSPT, updateAksiSPT, createPDFSPT, getPDFSPT } from '../../controllers/SPT/SPTController.js'

const router = express.Router()

router.get('/', getSPT)
router.get('/:no', getSPTByNoOrder)
router.post('/', createSPT)
router.post('/create-pdf', createPDFSPT)
router.get('/fetch-pdf/:no', getPDFSPT)
router.put("/:no", updateSPT)
router.put("/aksi/:no", updateAksiSPT)

export default router;