import express from 'express'
import { getRevisi, getRevisiByIdUttp, createRevisi, updateRevisi } from '../../controllers/SKHP/RevisiSKHPController.js'

const router = express.Router()

router.get('/', getRevisi)
router.get('/:id', getRevisiByIdUttp)
router.post('/', createRevisi)
router.put('/:id', updateRevisi)

export default router;