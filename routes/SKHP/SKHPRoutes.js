import express from 'express'
import { getSKHP, createSKHP, updateSKHP } from '../../controllers/SKHP/SKHPController.js'

const router = express.Router()

router.get('/', getSKHP)
router.post('/', createSKHP)
router.put("/:id", updateSKHP)

export default router;