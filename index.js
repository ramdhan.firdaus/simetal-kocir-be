import express from 'express'
import bodyParser from 'body-parser'
import dotenv from "dotenv";
import cors from 'cors'
import path from 'path'
import cookieParser from "cookie-parser";
import { fileURLToPath } from 'url'
import akunRoutes from './routes/AkunRoutes.js'
import routes from './routes/LoginRoutes.js'
import roleRoutes from './routes/RoleRoutes.js'

// Pengaduan
import buktiPengaduanRoutes from './routes/Pengaduan/BuktiPengaduanRoutes.js'
import pengaduanRoutes from './routes/Pengaduan/PengaduanRoutes.js'

// SPT
import petugasSPTRoutes from './routes/SPT/PetugasSPTRoutes.js'
import sptRoutes from './routes/SPT/SPTRoutes.js'
import revisiSPTRoutes from './routes/SPT/RevisiSPTRoutes.js'

// Tera
import jenisUTTPRoutes from './routes/Tera/Pendaftaran Tera/JenisUTTPRoutes.js'
import subJenisUTTPRoutes from './routes/Tera/Pendaftaran Tera/JenisUTTPSubRoutes.js'
import uttpRoutes from './routes/Tera/Pendaftaran Tera/UTTPRoutes.js'
import pendaftaranTeraRoutes from './routes/Tera/Pendaftaran Tera/PendaftaranTeraRoutes.js'
import kecamatanRoutes from './routes/Tera/Pendaftaran Tera/KecamatanRoutes.js'
import kelurahanRoutes from './routes/Tera/Pendaftaran Tera/KelurahanRoutes.js'
import satuanKapasitasRoutes from './routes/Tera/Pendaftaran Tera/SatuanKapasitasRoutes.js'
import pelayananTeraRoutes from './routes/Tera/Pelayanan Tera/PelayananTeraRoutes.js'
import pengamatanTeraRoutes from './routes/Tera/Pengamatan Tera/PengamatanTeraRoutes.js'
import pengawasanTeraRoutes from './routes/Tera/Pengawasan Tera/PengawasanTeraRoutes.js'

import skhpRoutes from './routes/SKHP/SKHPRoutes.js'
import revisiSKHPRoutes from './routes/SKHP/RevisiSKHPRoutes.js'

import targetRetribusiRoutes from './routes/Pelaporan/TargetRetribusiRoutes.js'
import pelaporanRoutes from './routes/Pelaporan/PelaporanRoutes.js'

dotenv.config();
const app = express()

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors({ credentials: true, origin: 'http://localhost:3001' }));
app.use(cookieParser());

app.use('/uploads', express.static(path.join(__dirname, 'uploads')))

app.use('/', routes)
app.use('/akun', akunRoutes)
app.use('/role', roleRoutes)
app.use('/bukti-pengaduan', buktiPengaduanRoutes)
app.use('/pengaduan', pengaduanRoutes)
app.use('/petugas-spt', petugasSPTRoutes)
app.use('/spt', sptRoutes)
app.use('/pendaftaran-tera', pendaftaranTeraRoutes)
app.use('/uttp', uttpRoutes)
app.use('/sub-jenis-uttp', subJenisUTTPRoutes)
app.use('/jenis-uttp', jenisUTTPRoutes)
app.use('/kecamatan', kecamatanRoutes)
app.use('/kelurahan', kelurahanRoutes)
app.use('/satuan-kapasitas', satuanKapasitasRoutes)
app.use('/pelayanan-tera', pelayananTeraRoutes)
app.use('/skhp', skhpRoutes)
app.use('/pengamatan-tera', pengamatanTeraRoutes)
app.use('/pengawasan-tera', pengawasanTeraRoutes)
app.use('/revisi-spt', revisiSPTRoutes)
app.use('/revisi-skhp', revisiSKHPRoutes)
app.use('/target-retribusi', targetRetribusiRoutes)
app.use('/pelaporan', pelaporanRoutes)

const PORT = process.env.PORT || 5000

app.listen(PORT, () => {
    console.log(`Server is listening on ${PORT}`)
})

export default __dirname