import { Sequelize } from "sequelize";
import db from "../config/Database.js";
import Role from "./RoleModel.js";

const { DataTypes } = Sequelize;

const Akun = db.define('akun', {
    username: {
        type: DataTypes.STRING,
        unique: true,
        required: true
    },
    email: {
        type: DataTypes.STRING,
        primaryKey: true,
        required: true
    },
    password: {
        type: DataTypes.STRING,
        required: true
    },
    nama: {
        type: DataTypes.STRING,
        required: true
    },
    nip: {
        type: DataTypes.STRING,
    },
    refresh_token: {
        type: DataTypes.TEXT
    }
}, {
    freezeTableName: true
});

Role.hasMany(Akun);
Akun.belongsTo(Role);

(async () => {
    await db.sync();
})();

export default Akun;