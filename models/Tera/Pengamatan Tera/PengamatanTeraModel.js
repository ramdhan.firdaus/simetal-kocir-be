import { Sequelize } from "sequelize";
import db from "../../../config/database.js";
import UTTP from "../Pendaftaran Tera/UTTPModel.js"

const { DataTypes } = Sequelize;

const PengamatanTera = db.define('pengamatan_tera', {
    tandaTera: {
        type: DataTypes.INTEGER,
        required: true
    },
    hasilPengamatan: {
        type: DataTypes.STRING,
        required: true
    }
}, {
    freezeTableName: true
});

UTTP.hasOne(PengamatanTera);
PengamatanTera.belongsTo(UTTP);

(async () => {
    await db.sync();
})();

export default PengamatanTera;