import { Sequelize } from "sequelize";
import db from "../../../config/database.js";
import UTTP from "../Pendaftaran Tera/UTTPModel.js"

const { DataTypes } = Sequelize;

const PelayananTera = db.define('pelayanan_tera', {
    status: {
        type: DataTypes.STRING,
        required: true
    },
    statusPembayaran: {
        type: DataTypes.STRING
    },
    inUji: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    inBayar: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    }
}, {
    freezeTableName: true
});

UTTP.hasOne(PelayananTera);
PelayananTera.belongsTo(UTTP);

(async () => {
    await db.sync();
})();

export default PelayananTera;