import { Sequelize } from "sequelize";
import db from "../../../config/Database.js";
import Kelurahan from './KelurahanModel.js'

const { DataTypes } = Sequelize;

const PendaftaranTera = db.define('pendaftaran_tera', {
    noOrder: {
        type: DataTypes.STRING,
        primaryKey: true,
        unique: true,
    },
    namaPemilik: {
        type: DataTypes.STRING,
    },
    alamatLengkap: {
        type: DataTypes.STRING,
    },
    jenisUsaha: {
        type: DataTypes.STRING,
    },
    noHP: {
        type: DataTypes.STRING,
    },
    tanggal: {
        type: DataTypes.DATE,
    }
}, {
    freezeTableName: true
});

Kelurahan.hasMany(PendaftaranTera);
PendaftaranTera.belongsTo(Kelurahan);

(async () => {
    await db.sync();
})();

export default PendaftaranTera;