import { Sequelize } from "sequelize";
import db from "../../../config/Database.js";
import PendaftaranTera from "./PendaftaranTeraModel.js";
import SubJenisUTTP from "./JenisUTTPSubModel.js";
import SatuanKapasitas from "./SatuanKapasitasModel.js";

const { DataTypes } = Sequelize;

const UTTP = db.define('uttp', {
    jenisPelayanan: {
        type: DataTypes.STRING
    },
    teraOrTeraUlang: {
        type: DataTypes.STRING
    },
    kapasitas: {
        type: DataTypes.INTEGER
    },
    merek: {
        type: DataTypes.STRING
    },
    model: {
        type: DataTypes.STRING
    },
    noSeri: {
        type: DataTypes.STRING
    },
    kelengkapanUTTP: {
        type: DataTypes.STRING
    },
    skhp: {
        type: DataTypes.STRING
    }
}, {
    freezeTableName: true
});

PendaftaranTera.hasMany(UTTP);
UTTP.belongsTo(PendaftaranTera);

SubJenisUTTP.hasMany(UTTP);
UTTP.belongsTo(SubJenisUTTP);

SatuanKapasitas.hasMany(UTTP);
UTTP.belongsTo(SatuanKapasitas);

(async () => {
    await db.sync();
})();

export default UTTP;