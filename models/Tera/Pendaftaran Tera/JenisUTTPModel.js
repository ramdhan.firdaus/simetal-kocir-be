import { Sequelize } from "sequelize";
import db from "../../../config/Database.js";

const { DataTypes } = Sequelize;

const JenisUTTP = db.define('jenis_uttp', {
    jenis: {
        type: DataTypes.STRING,
        unique: true,
    }
}, {
    freezeTableName: true
});

(async () => {
    await db.sync();
})();

export default JenisUTTP;