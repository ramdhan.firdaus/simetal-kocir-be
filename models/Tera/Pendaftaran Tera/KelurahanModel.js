import { Sequelize } from "sequelize";
import db from "../../../config/Database.js";
import Kecamatan from "./KecamatanModel.js";

const { DataTypes } = Sequelize;

const Kelurahan = db.define('kelurahan', {
    nama: {
        type: DataTypes.STRING,
        unique: true,
    }
}, {
    freezeTableName: true
});

Kecamatan.hasMany(Kelurahan);
Kelurahan.belongsTo(Kecamatan);

(async () => {
    await db.sync();
})();

export default Kelurahan;