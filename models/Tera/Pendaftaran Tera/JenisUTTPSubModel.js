import { Sequelize } from "sequelize";
import db from "../../../config/Database.js";
import JenisUTTP from "./JenisUTTPModel.js";

const { DataTypes } = Sequelize;

const SubJenisUTTP = db.define('sub_jenis_uttp', {
    jenis: {
        type: DataTypes.STRING,
        unique: true,
    },
    retribusi: {
        type: DataTypes.INTEGER,
        unique: true,
    }
}, {
    freezeTableName: true
});

JenisUTTP.hasMany(SubJenisUTTP);
SubJenisUTTP.belongsTo(JenisUTTP);

(async () => {
    await db.sync();
})();

export default SubJenisUTTP;