import { Sequelize } from "sequelize";
import db from "../../../config/Database.js";

const { DataTypes } = Sequelize;

const Kecamatan = db.define('kecamatan', {
    nama: {
        type: DataTypes.STRING,
        unique: true,
    }
}, {
    freezeTableName: true
});

(async () => {
    await db.sync();
})();

export default Kecamatan;