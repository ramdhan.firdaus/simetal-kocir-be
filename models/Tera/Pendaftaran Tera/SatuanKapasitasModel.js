import { Sequelize } from "sequelize";
import db from "../../../config/Database.js";

const { DataTypes } = Sequelize;

const SatuanKapasitas = db.define('satuan_kapasitas', {
    nama: {
        type: DataTypes.STRING,
        unique: true,
    }
}, {
    freezeTableName: true
});

(async () => {
    await db.sync();
})();

export default SatuanKapasitas;