import { Sequelize } from "sequelize";
import db from "../../../config/database.js";
import UTTP from "../Pendaftaran Tera/UTTPModel.js"

const { DataTypes } = Sequelize;

const PengawasanTera = db.define('pengawasan_tera', {
    tandaTera: {
        type: DataTypes.INTEGER,
        required: true
    },
    hasilPengawasan: {
        type: DataTypes.STRING,
        required: true
    }
}, {
    freezeTableName: true
});

UTTP.hasOne(PengawasanTera);
PengawasanTera.belongsTo(UTTP);

(async () => {
    await db.sync();
})();

export default PengawasanTera;