import { Sequelize } from "sequelize";
import db from "../../config/Database.js";

const { DataTypes } = Sequelize;

const TargetRetribusi = db.define('target_retribusi', {
    tahun: {
        type: DataTypes.INTEGER,
        unique: true,
    },
    target: {
        type: DataTypes.INTEGER,
        unique: true,
    }
}, {
    freezeTableName: true
});

(async () => {
    await db.sync();
})();

export default TargetRetribusi;