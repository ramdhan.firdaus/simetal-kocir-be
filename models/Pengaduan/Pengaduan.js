import { Sequelize } from "sequelize";
import db from "../../config/Database.js";
import Akun from "../AkunModel.js"

const { DataTypes } = Sequelize;

const Pengaduan = db.define('pengaduan', {
    nama: {
        type: DataTypes.STRING,
        required: true
    },
    alamat: {
        type: DataTypes.STRING,
        required: true
    },
    noHP: {
        type: DataTypes.STRING,
        required: true
    },
    uraian: {
        type: DataTypes.STRING,
        required: true
    }
}, {
    freezeTableName: true
});

Akun.hasMany(Pengaduan);
Pengaduan.belongsTo(Akun);

(async () => {
    await db.sync();
})();

export default Pengaduan;