import { Sequelize } from "sequelize";
import db from "../../config/Database.js";
import Pengaduan from "./Pengaduan.js";

const { DataTypes } = Sequelize;

const BuktiPengaduan = db.define('bukti_pengaduan', {
    bukti: {
        type: DataTypes.STRING,
        unique: true,
    }
}, {
    freezeTableName: true
});

Pengaduan.hasMany(BuktiPengaduan);
BuktiPengaduan.belongsTo(Pengaduan);

(async () => {
    await db.sync();
})();

export default BuktiPengaduan;