import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const { DataTypes } = Sequelize;

const Role = db.define('role', {
    nama: {
        type: DataTypes.STRING,
        required: true
    },
}, {
    freezeTableName: true
});

(async () => {
    await db.sync();
})();

export default Role;