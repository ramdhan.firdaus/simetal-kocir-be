import { Sequelize } from "sequelize";
import db from "../../config/Database.js";
import SKHP from "./SKHPModel.js";

const { DataTypes } = Sequelize;

const Revisi = db.define('revisi_skhp', {
    detail: {
        type: DataTypes.STRING,
        required: true
    },
    namaPerevisi: {
        type: DataTypes.STRING,
        required: true
    },
    rolePerevisi: {
        type: DataTypes.STRING,
        required: true
    },
    status: {
        type: DataTypes.STRING,
        required: true
    }
}, {
    freezeTableName: true
});

SKHP.hasMany(Revisi);
Revisi.belongsTo(SKHP);

(async () => {
    await db.sync();
})();

export default Revisi;