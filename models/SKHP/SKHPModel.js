import { Sequelize } from "sequelize";
import db from "../../config/database.js";
import UTTP from "../Tera/Pendaftaran Tera/UTTPModel.js"

const { DataTypes } = Sequelize;

const PenerbitanSKHP = db.define('penerbitan_skhp', {
    uttpId: {
        type: DataTypes.INTEGER,
        primaryKey: true
    },
    status: {
        type: DataTypes.STRING,
        required: true
    }
}, {
    freezeTableName: true
});

UTTP.hasOne(PenerbitanSKHP);
PenerbitanSKHP.belongsTo(UTTP);

(async () => {
    await db.sync();
})();

export default PenerbitanSKHP;