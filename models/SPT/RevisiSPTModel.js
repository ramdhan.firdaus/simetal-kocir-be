import { Sequelize } from "sequelize";
import db from "../../config/Database.js";
import SPT from "./SPTModel.js";

const { DataTypes } = Sequelize;

const Revisi = db.define('revisi_spt', {
    detail: {
        type: DataTypes.STRING,
        required: true
    },
    namaPerevisi: {
        type: DataTypes.STRING,
        required: true
    },
    rolePerevisi: {
        type: DataTypes.STRING,
        required: true
    },
    status: {
        type: DataTypes.STRING,
        required: true
    }
}, {
    freezeTableName: true
});

SPT.hasMany(Revisi);
Revisi.belongsTo(SPT);

(async () => {
    await db.sync();
})();

export default Revisi;