import { Sequelize } from "sequelize";
import db from "../../config/Database.js";
import PendaftaranTera from "../Tera/Pendaftaran Tera/PendaftaranTeraModel.js";

const { DataTypes } = Sequelize;

const SPT = db.define('spt', {
    pendaftaranTeraNoOrder: {
        type: DataTypes.STRING,
        primaryKey: true
    },
    pemohon: {
        type: DataTypes.STRING,
        required: true
    },
    tanggalPelaksanaan: {
        type: DataTypes.DATE,
        required: true
    },
    alamat: {
        type: DataTypes.STRING,
        required: true
    },
    status: {
        type: DataTypes.STRING,
        required: true
    },
}, {
    freezeTableName: true
});

PendaftaranTera.hasOne(SPT);
SPT.belongsTo(PendaftaranTera);

(async () => {
    await db.sync();
})();

export default SPT;