import { Sequelize } from "sequelize";
import db from "../../config/Database.js";
import SPT from "./SPTModel.js";

const { DataTypes } = Sequelize;

const PetugasSPT = db.define('petugas_spt', {
    nip: {
        type: DataTypes.STRING,
        required: true
    },
    nama: {
        type: DataTypes.STRING,
        required: true
    }
}, {
    freezeTableName: true
});

SPT.hasMany(PetugasSPT);
PetugasSPT.belongsTo(SPT);

(async () => {
    await db.sync();
})();

export default PetugasSPT;