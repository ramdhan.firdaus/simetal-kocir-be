import Akun from "../models/AkunModel.js";
import Role from "../models/RoleModel.js"
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

export const getAkun = async (req, res) => {
    try {
        const akun = await Akun.findAll({
            where: {
                refresh_token: req.cookies.refreshToken
            },
            include: [
                {
                    model: Role,
                    as: 'role',
                    attributes: ['nama']
                }
            ]
        });
        res.json(akun);
    } catch (error) {
        console.log(error);
    }
}

export const getAllAkun = async (req, res) => {
    try {
        const akun = await Akun.findAll({
            include: [
                {
                    model: Role,
                    as: 'role',
                    attributes: ['nama']
                }
            ],
            attributes: ['nama', 'nip', 'username']
        });
        res.json(akun);
    } catch (error) {
        console.log(error);
    }
}

export const getAkunPetugas = async (req, res) => {
    try {
        const akun = await Akun.findAll({
            where: {
                roleId: 6
            }
        });
        res.json(akun);
    } catch (error) {
        console.log(error);
    }
}

export const RegisterAkun = async (req, res) => {
    const { nama, username, email, password } = req.body;
    if (username === undefined || email === undefined) return res.status(400).json({ msg: "Email atau Username tidak boleh kosong" });
    if (username === '' || email === '') return res.status(400).json({ msg: "Email atau Username tidak boleh kosong" });
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);
    const role = await Role.findOne({
        where: {
            nama: "PENGGUNA"
        }
    });
    try {
        await Akun.create({
            nama: nama,
            username: username,
            email: email,
            password: hashPassword,
            roleId: role.id,
            nip: ''
        });
        res.json({ msg: "Register Berhasil" });
    } catch (error) {
        res.status(400).json({ msg: "Email telah digunakan" });
    }
}

export const Login = async (req, res) => {
    try {
        const akun = await Akun.findAll({
            where: {
                username: req.body.username
            }
        });
        const match = await bcrypt.compare(req.body.password, akun[0].password);
        if (!match) return res.status(400).json({ msg: "Password salah. Harap cek kembali" });
        const name = akun[0].name;
        const email = akun[0].email;
        const accessToken = jwt.sign({ name, email }, process.env.ACCESS_TOKEN_SECRET, {
            expiresIn: '20s'
        });
        const refreshToken = jwt.sign({ name, email }, process.env.REFRESH_TOKEN_SECRET, {
            expiresIn: '1d'
        });
        await Akun.update({ refresh_token: refreshToken }, {
            where: {
                email: email
            }
        });
        res.cookie('refreshToken', refreshToken, {
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000
        });
        res.json({ accessToken });
    } catch (error) {
        res.status(404).json({ msg: "Username salah. Harap cek kembali" });
    }
}

export const Logout = async (req, res) => {
    const refreshToken = req.cookies.refreshToken;
    if (!refreshToken) return res.sendStatus(204);
    const akun = await Akun.findAll({
        where: {
            refresh_token: refreshToken
        }
    });
    if (!akun[0]) return res.sendStatus(204);
    const email = akun[0].email;
    await Akun.update({ refresh_token: null }, {
        where: {
            email: email
        }
    });
    res.clearCookie('refreshToken');
    return res.sendStatus(200);
}

export const updateAkun = async (req, res) => {
    try {
        const pengguna = req.body.pengguna
        const role = req.body.role
        const nip = req.body.nip
        pengguna.forEach(async (element, i) => {
            const akun = await Akun.findOne({
                where: {
                    username: element.username
                }
            });
            if (role[i] !== '') akun.roleId = role[i]
            if (nip[i] !== '') akun.nip = nip[i]
            akun.save()
        });
        res.json({ msg: "Update Akun Berhasil" });
    } catch (error) {
        console.log(error);
    }
}