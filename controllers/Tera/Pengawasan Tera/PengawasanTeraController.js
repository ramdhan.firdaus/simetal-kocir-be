import PengawasanTera from "../../../models/Tera//Pengawasan Tera/PengawasanTeraModel.js"
import UTTP from "../../../models/Tera/Pendaftaran Tera/UTTPModel.js";
import PendaftaranTera from "../../../models/Tera/Pendaftaran Tera/PendaftaranTeraModel.js";
import SatuanKapasitas from "../../../models/Tera/Pendaftaran Tera/SatuanKapasitasModel.js";
import SubJenisUTTP from "../../../models/Tera/Pendaftaran Tera/JenisUTTPSubModel.js";

export const getPengawasanTera = async (req, res) => {
    try {
        const pengawasan = await PengawasanTera.findAll({
            include: [
                {
                    model: UTTP,
                    as: 'uttp',
                    include: [
                        {
                            model: PendaftaranTera,
                            as: 'pendaftaran_tera',
                            attributes: ["noOrder", "namaPemilik", "alamatLengkap"]
                        },
                        {
                            model: SatuanKapasitas,
                            as: 'satuan_kapasita',
                            attributes: ["nama"]
                        },
                        {
                            model: SubJenisUTTP,
                            as: 'sub_jenis_uttp',
                            attributes: ["jenis"]
                        }
                    ],
                    attributes: ["kapasitas", "merek", "model", "noSeri"],
                }
            ],
            attributes: ["tandaTera", "hasilPengawasan", "uttpId"],
        });
        res.json(pengawasan);
    } catch (error) {
        console.log(error);
    }
}

export const getPengawasanTeraById = async (req, res) => {
    try {
        const pengawasan = await PengawasanTera.findAll({
            where: {
                id: req.params.id
            }
        });
        res.json(pengawasan);
    } catch (error) {
        console.log(error);
    }
}

export const createPengawasanTera = async (req, res) => {
    try {
        const uttp = await UTTP.findAll({
            pendaftaranTeraNoOrder: req.body.noOrder
        })
        uttp.forEach(element => {
            PengawasanTera.create({
                uttpId: element.id,
                tandaTera: req.body.tandaTera,
                hasilPengawasan: req.body.hasil
            })
        });
        res.json({ msg: "Tambah Data Pengawasan Tera Berhasil" });
    } catch (error) {

    }
}