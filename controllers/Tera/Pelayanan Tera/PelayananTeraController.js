import PelayananTera from "../../../models/Tera//Pelayanan Tera/PelayananTeraModel.js"
import UTTP from "../../../models/Tera/Pendaftaran Tera/UTTPModel.js";
import PendaftaranTera from "../../../models/Tera/Pendaftaran Tera/PendaftaranTeraModel.js";
import SatuanKapasitas from "../../../models/Tera/Pendaftaran Tera/SatuanKapasitasModel.js";
import SubJenisUTTP from "../../../models/Tera/Pendaftaran Tera/JenisUTTPSubModel.js";

export const getPelayananTera = async (req, res) => {
    try {
        const pelayanan = await PelayananTera.findAll({
            include: [
                {
                    model: UTTP,
                    as: 'uttp',
                    include: [
                        {
                            model: PendaftaranTera,
                            as: 'pendaftaran_tera',
                            attributes: ["noOrder", "namaPemilik", "alamatLengkap"]
                        },
                        {
                            model: SatuanKapasitas,
                            as: 'satuan_kapasita',
                            attributes: ["nama"]
                        },
                        {
                            model: SubJenisUTTP,
                            as: 'sub_jenis_uttp',
                            attributes: ["jenis", "retribusi"]
                        }
                    ],
                    attributes: ["kapasitas", "merek", "model", "noSeri"],
                }
            ],
            attributes: ["status", "statusPembayaran", "uttpId"],
        });
        res.json(pelayanan);
    } catch (error) {
        console.log(error);
    }
}

export const getPelayananTeraUji = async (req, res) => {
    try {
        const pelayanan = await PelayananTera.findAll({
            where: {
                inUji: 1
            },
            include: [
                {
                    model: UTTP,
                    as: 'uttp',
                    include: [
                        {
                            model: PendaftaranTera,
                            as: 'pendaftaran_tera',
                            attributes: ["noOrder", "namaPemilik", "alamatLengkap"]
                        },
                        {
                            model: SatuanKapasitas,
                            as: 'satuan_kapasita',
                            attributes: ["nama"]
                        },
                        {
                            model: SubJenisUTTP,
                            as: 'sub_jenis_uttp',
                            attributes: ["jenis", "retribusi"]
                        }
                    ],
                    attributes: ["kapasitas", "merek", "model", "noSeri"],
                }
            ],
            attributes: ["status", "statusPembayaran", "uttpId"],
        });
        res.json(pelayanan);
    } catch (error) {
        console.log(error);
    }
}

export const getPelayananTeraUjiBayar = async (req, res) => {
    try {
        const pelayanan = await PelayananTera.findAll({
            where: {
                inBayar: 1
            },
            include: [
                {
                    model: UTTP,
                    as: 'uttp',
                    include: [
                        {
                            model: PendaftaranTera,
                            as: 'pendaftaran_tera',
                            attributes: ["noOrder", "namaPemilik", "alamatLengkap"]
                        },
                        {
                            model: SatuanKapasitas,
                            as: 'satuan_kapasita',
                            attributes: ["nama"]
                        },
                        {
                            model: SubJenisUTTP,
                            as: 'sub_jenis_uttp',
                            attributes: ["jenis", "retribusi"]
                        }
                    ],
                    attributes: ["kapasitas", "merek", "model", "noSeri"],
                }
            ],
            attributes: ["status", "statusPembayaran", "uttpId"],
        });
        res.json(pelayanan);
    } catch (error) {
        console.log(error);
    }
}

export const createPelayananTera = async (req, res) => {
    try {
        await req.body.uttpId.forEach(element => {
            PelayananTera.create({
                status: "MENUNGGU KAJI ULANG",
                uttpId: element.id
            })
        });
        res.json({ msg: "Pelayanan UTTP Berhasil" });
    } catch (error) {
        console.log(error);
    }
}

export const updatePelayananTera = async (req, res) => {
    try {
        const pelayanan = await PelayananTera.findOne({
            where: {
                uttpId: req.params.id
            }
        })
        pelayanan.status = req.body.status
        if (req.body.status === 'SELESAI KAJI ULANG') {
            pelayanan.inUji = 1
        } else if (req.body.status = 'PENGUJIAN SELESAI') {
            pelayanan.inBayar = 1
            pelayanan.statusPembayaran = 'BELUM DIBAYAR'
        }
        pelayanan.save()
        res.json({ msg: "Update Pelayanan Tera Berhasil" });
    } catch (error) {
        console.log(error);
    }
}

export const bayarPelayananTera = async (req, res) => {
    try {
        const pelayanan = await PelayananTera.findOne({
            where: {
                uttpId: req.params.id
            }
        })
        pelayanan.statusPembayaran = req.body.statusPembayaran
        pelayanan.save()
        res.json({ msg: "Pembayaran Pelayanan Tera Berhasil" });
    } catch (error) {
        console.log(error);
    }
}