import Kelurahan from "../../../models/Tera/Pendaftaran Tera/KelurahanModel.js"

export const getKelurahan = async (req, res) => {
    try {
        const kelurahan = await Kelurahan.findAll({
            attributes: ["id", "nama"]
        });
        res.json(kelurahan);
    } catch (error) {
        console.log(error);
    }
}

export const getKelurahanByIdKecamatan = async (req, res) => {
    try {
        const kelurahan = await Kelurahan.findAll({
            where: { kecamatanId: req.params.kecamatan },
            attributes: ["id", "nama"]
        });
        res.json(kelurahan);
    } catch (error) {
        console.log(error);
    }
}