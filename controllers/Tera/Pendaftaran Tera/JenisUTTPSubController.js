import SubJenisUTTP from "../../../models/Tera/Pendaftaran Tera/JenisUTTPSubModel.js"
import JenisUTTP from "../../../models/Tera/Pendaftaran Tera/JenisUTTPModel.js";

export const getJenisUTTP = async (req, res) => {
    try {
        const jenis = await SubJenisUTTP.findAll({
            attributes: ["id", "jenis", "retribusi", "jenisUTTPId"],
            include: [
                {
                    model: JenisUTTP,
                    as: 'jenis_uttp',
                    attributes: ['jenis']
                }
            ]
        });
        res.json(jenis);
    } catch (error) {
        console.log(error);
    }
}