import Kecamatan from "../../../models/Tera/Pendaftaran Tera/KecamatanModel.js"

export const getKecamatan = async (req, res) => {
    try {
        const kecamatan = await Kecamatan.findAll({
            attributes: ["id", "nama"]
        });
        res.json(kecamatan);
    } catch (error) {
        console.log(error);
    }
}