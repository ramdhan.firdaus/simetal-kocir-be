import UTTP from "../../../models/Tera/Pendaftaran Tera/UTTPModel.js"
import SubJenisUTTP from "../../../models/Tera/Pendaftaran Tera/JenisUTTPSubModel.js";
import SatuanKapasitas from "../../../models/Tera/Pendaftaran Tera/SatuanKapasitasModel.js";
import sequelize from "sequelize";

export const getUTTP = async (req, res) => {
    try {
        const akun = await UTTP.findAll();
        res.json(akun);
    } catch (error) {
        console.log(error);
    }
}

export const getUTTPById = async (req, res) => {
    try {
        const uttp = await UTTP.findOne({
            where: { id: req.params.id },
            include: [
                {
                    model: SubJenisUTTP,
                    as: 'sub_jenis_uttp',
                    attributes: ['jenis', 'retribusi'],
                },
                {
                    model: SatuanKapasitas,
                    as: 'satuan_kapasita',
                    attributes: ['nama']
                }
            ],
            attributes: ["id", "jenisPelayanan", "teraOrTeraUlang", "kapasitas",
                "merek", "model", "noSeri", "kelengkapanUTTP", "skhp"]
        });
        res.json(uttp);
    } catch (error) {
        console.log(error);
    }
}

export const getUTTPByTahun = async (req, res) => {
    try {
        const tahun = parseInt(req.params.tahun)
        const uttp = await UTTP.findAll({
            where: {
                createdAt: {
                    [sequelize.Op.lt]: new Date(`${tahun + 1}`),
                    [sequelize.Op.gt]: new Date(`${tahun}`)
                }
            },
        });
        res.json(uttp);
    } catch (error) {
        console.log(error);
    }
}

export const createUTTP = async (req, res) => {
    try {
        const dataUttp = []
        let data = req.body
        for (let i = 0; i < data.jenisPelayanan.length; i++) {
            const uttp = await UTTP.create({
                pendaftaranTeraNoOrder: data.noOrder,
                jenisPelayanan: data.jenisPelayanan[i],
                teraOrTeraUlang: data.teraTeraUlang[i],
                subJenisUttpId: data.subJenisUTTP[i],
                kapasitas: data.kapasitas[i],
                satuanKapasitaId: data.satuanKapasitas[i],
                merek: data.merek[i],
                model: data.model[i],
                noSeri: data.noSeri[i],
                kelengkapanUTTP: data.kelengkapanUTTP[i],
                skhp: data.SKHP[i],
            });
            dataUttp.push(uttp)
        }
        res.json({ msg: "Pendaftaran UTTP Berhasil", data: dataUttp });
    } catch (error) {
        console.log(error);
    }
}

export const updateUTTP = async (req, res) => {
    try {
        const uttp = await UTTP.findOne({
            where: {
                id: req.params.id
            }
        })
        let data = req.body.data
        uttp.jenisPelayanan = data.jenisPelayanan
        uttp.teraOrTeraUlang = data.teraOrTeraUlang
        uttp.subJenisUttpId = data.subJenisUttpId
        uttp.kapasitas = data.kapasitas
        uttp.satuanKapasitaId = data.satuanKapasitaId
        uttp.merek = data.merek
        uttp.model = data.model
        uttp.noSeri = data.noSeri
        uttp.kelengkapanUTTP = data.kelengkapanUTTP
        uttp.skhp = data.skhp
        await uttp.save()
        res.json({ msg: "Update UTTP Berhasil" });
    } catch (error) {
        console.log(error);
    }
}