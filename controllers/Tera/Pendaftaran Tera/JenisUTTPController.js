import JenisUTTP from "../../../models/Tera/Pendaftaran Tera/JenisUTTPModel.js"

export const getJenisUTTP = async (req, res) => {
    try {
        const jenis = await JenisUTTP.findAll({
            attributes: ["id", "jenis"]
        });
        res.json(jenis);
    } catch (error) {
        console.log(error);
    }
}