import PendaftaranTera from "../../../models/Tera/Pendaftaran Tera/PendaftaranTeraModel.js"
import SPT from "../../../models/SPT/SPTModel.js";
import { Op } from "sequelize";
import Kecamatan from "../../../models/Tera/Pendaftaran Tera/KecamatanModel.js";
import Kelurahan from "../../../models/Tera/Pendaftaran Tera/KelurahanModel.js";
import PengamatanTera from "../../../models/Tera/Pengamatan Tera/PengamatanTeraModel.js"
import PengawasanTera from "../../../models/Tera/Pengawasan Tera/PengawasanTeraModel.js"
import UTTP from "../../../models/Tera/Pendaftaran Tera/UTTPModel.js";

export const getPendaftaranTera = async (req, res) => {
    try {
        const akun = await PendaftaranTera.findAll();
        res.json(akun);
    } catch (error) {
        console.log(error);
    }
}

export const getPendaftaranTeraNotSPT = async (req, res) => {
    try {
        let dataSPT = []
        const spt = await SPT.findAll({});
        spt.forEach((element, i) => {
            dataSPT.push(element.pendaftaranTeraNoOrder)
        })

        const pendaftaran = await PendaftaranTera.findAll({
            where: {
                noOrder: {
                    [Op.notIn]: dataSPT
                }
            }
        });

        res.json(pendaftaran);
    } catch (error) {
        console.log(error);
    }
}

export const getPendaftaranTeraNotPengamatan = async (req, res) => {
    try {
        console.log("asasas")
        let dataPengamatan = []
        const pengamatan = await PengamatanTera.findAll({
            include: [
                {
                    model: UTTP,
                    as: 'uttp',
                    attributes: ['pendaftaranTeraNoOrder']
                }
            ]
        });
        console.log("asasas")
        pengamatan.forEach((element, i) => {
            dataPengamatan.push(element.uttp.pendaftaranTeraNoOrder)
        })

        const pendaftaran = await PendaftaranTera.findAll({
            where: {
                noOrder: {
                    [Op.notIn]: dataPengamatan
                }
            }
        });

        res.json(pendaftaran);
    } catch (error) {
        console.log(error);
    }
}

export const getPendaftaranTeraNotPengawasan = async (req, res) => {
    try {
        let dataPengawasan = []
        const pengawasan = await PengawasanTera.findAll({
            include: [
                {
                    model: UTTP,
                    as: 'uttp',
                    attributes: ['pendaftaranTeraNoOrder']
                }
            ]
        });
        pengawasan.forEach((element, i) => {
            dataPengawasan.push(element.uttp.pendaftaranTeraNoOrder)
        })

        const pendaftaran = await PendaftaranTera.findAll({
            where: {
                noOrder: {
                    [Op.notIn]: dataPengawasan
                }
            }
        });

        res.json(pendaftaran);
    } catch (error) {
        console.log(error);
    }
}

export const getPendaftaranTeraByNo = async (req, res) => {
    try {
        const pendaftaran = await PendaftaranTera.findOne({
            where: { noOrder: req.params.no },
            include: [
                {
                    model: Kelurahan,
                    as: 'kelurahan',
                    attributes: ['nama'],
                    include: [
                        {
                            model: Kecamatan,
                            as: 'kecamatan',
                            attributes: ['nama']
                        }
                    ]
                }
            ],
            attributes: ["noOrder", "tanggal", "namaPemilik", "alamatLengkap",
                "jenisUsaha", "noHP"]
        });
        res.json(pendaftaran);
    } catch (error) {
        console.log(error);
    }
}

export const createPendaftaran = async (req, res) => {
    try {
        await PendaftaranTera.create({
            noOrder: req.body.noOrder,
            namaPemilik: req.body.nama,
            kelurahanId: req.body.kelurahan,
            alamatLengkap: req.body.alamatLengkap,
            jenisUsaha: req.body.jenisUsaha,
            noHP: req.body.noHP,
            tanggal: req.body.tanggal
        });
        res.json({ msg: "Pendaftaran Tera Berhasil" });
    } catch (error) {
        console.log(error);
    }
}

export const updatePendaftaran = async (req, res) => {
    try {
        const pendaftaran = await PendaftaranTera.findOne({
            where: {
                noOrder: req.params.no
            }
        })
        pendaftaran.namaPemilik = req.body.namaPemilik
        pendaftaran.alamatLengkap = req.body.alamatLengkap
        pendaftaran.kelurahanId = req.body.kelurahanId
        pendaftaran.jenisUsaha = req.body.jenisUsaha
        pendaftaran.noHP = req.body.noHP
        pendaftaran.save()
        res.json({ msg: "Update Tera Berhasil" });
    } catch (error) {
        console.log(error);
    }
}