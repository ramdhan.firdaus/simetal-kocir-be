import SatuanKapasitas from "../../../models/Tera/Pendaftaran Tera/SatuanKapasitasModel.js"

export const getSatuanKapasitas = async (req, res) => {
    try {
        const satuanKapasitas = await SatuanKapasitas.findAll({
            attributes: ["id", "nama"]
        });
        res.json(satuanKapasitas);
    } catch (error) {
        console.log(error);
    }
}