import PengamatanTera from "../../../models/Tera//Pengamatan Tera/PengamatanTeraModel.js"
import UTTP from "../../../models/Tera/Pendaftaran Tera/UTTPModel.js";
import PendaftaranTera from "../../../models/Tera/Pendaftaran Tera/PendaftaranTeraModel.js";
import SatuanKapasitas from "../../../models/Tera/Pendaftaran Tera/SatuanKapasitasModel.js";
import SubJenisUTTP from "../../../models/Tera/Pendaftaran Tera/JenisUTTPSubModel.js";

export const getPengamatanTera = async (req, res) => {
    try {
        const pengamatan = await PengamatanTera.findAll({
            include: [
                {
                    model: UTTP,
                    as: 'uttp',
                    include: [
                        {
                            model: PendaftaranTera,
                            as: 'pendaftaran_tera',
                            attributes: ["noOrder", "namaPemilik", "alamatLengkap"]
                        },
                        {
                            model: SatuanKapasitas,
                            as: 'satuan_kapasita',
                            attributes: ["nama"]
                        },
                        {
                            model: SubJenisUTTP,
                            as: 'sub_jenis_uttp',
                            attributes: ["jenis"]
                        }
                    ],
                    attributes: ["kapasitas", "merek", "model", "noSeri"],
                }
            ],
            attributes: ["tandaTera", "hasilPengamatan", "uttpId"],
        });
        res.json(pengamatan);
    } catch (error) {
        console.log(error);
    }
}

export const getPengamatanTeraById = async (req, res) => {
    try {
        const pengamatan = await PengamatanTera.findAll({
            where: {
                id: req.params.id
            }
        });
        res.json(pengamatan);
    } catch (error) {
        console.log(error);
    }
}

export const createPengamatanTera = async (req, res) => {
    try {
        const uttp = await UTTP.findAll({
            pendaftaranTeraNoOrder: req.body.noOrder
        })
        uttp.forEach(element => {
            PengamatanTera.create({
                uttpId: element.id,
                tandaTera: req.body.tandaTera,
                hasilPengamatan: req.body.hasil
            })
        });
        res.json({ msg: "Tambah Data Pengamatan Tera Berhasil" });
    } catch (error) {

    }
}