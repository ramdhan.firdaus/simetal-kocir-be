import BuktiPengaduan from "../../models/Pengaduan/BuktiPengaduan.js"

export const getBuktiPengaduan = async (req, res) => {
    try {
        const bukti = await BuktiPengaduan.findAll({
            attributes: ["bukti"]
        });
        res.json(bukti);
    } catch (error) {
        console.log(error);
    }
}

export const getPengaduanByIDPengaduan = async (req, res) => {
    try {
        const bukti = await BuktiPengaduan.findAll({
            where: { pengaduanId: req.params.id },
            attributes: ["bukti"]
        });
        res.json(bukti);
    } catch (error) {
        console.log(error);
    }
}

export const createBuktiPengaduan = async (req, res) => {
    try {
        console.log(req.files)
        await req.files.forEach(element => {
            BuktiPengaduan.create({
                bukti: element.path,
                pengaduanId: req.params.id
            });
        });
        res.json({ msg: "Pengaduan Berhasil" });
    } catch (error) {
        console.log(error);
    }
}