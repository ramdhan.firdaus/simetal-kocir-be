import Pengaduan from "../../models/Pengaduan/Pengaduan.js";

export const getPengaduan = async (req, res) => {
    try {
        const pengaduan = await Pengaduan.findAll({
            attributes: ["id", "nama", "alamat", "noHP",
                "uraian", "akunEmail", "createdAt"]
        });
        res.json(pengaduan);
    } catch (error) {
        console.log(error);
    }
}

export const getPengaduanByID = async (req, res) => {
    try {
        const pengaduan = await Pengaduan.findOne({
            where: { id: req.params.id }
        });
        res.json(pengaduan);
    } catch (error) {
        console.log(error);
    }
}

export const createPengaduan = async (req, res) => {
    try {
        const pengaduan = await Pengaduan.create({
            akunEmail: req.body.email,
            nama: req.body.nama,
            alamat: req.body.alamat,
            noHP: req.body.noHp,
            uraian: req.body.uraian,
            status: 'DILAPORKAN'
        });
        res.json({ msg: "Pengaduan Berhasil", id: pengaduan.id });
    } catch (error) {
        console.log(error);
    }
}

export const updatePengaduan = async (req, res) => {
    try {
        await Pengaduan.updateOne(
            { id: req.params.id },
            {
                status: "DICEK"
            })
        res.json({ msg: "Pengaduan Diupdate" });
    } catch (error) {
        console.log(error);
    }
}