import PenerbitanSKHP from "../../models/SKHP/SKHPModel.js";
import UTTP from "../../models/Tera/Pendaftaran Tera/UTTPModel.js";
import PendaftaranTera from "../../models/Tera/Pendaftaran Tera/PendaftaranTeraModel.js";
import SatuanKapasitas from "../../models/Tera/Pendaftaran Tera/SatuanKapasitasModel.js";
import SubJenisUTTP from "../../models/Tera/Pendaftaran Tera/JenisUTTPSubModel.js";
import sequelize from "sequelize";
import Kelurahan from "../../models/Tera/Pendaftaran Tera/KelurahanModel.js";
import Kecamatan from "../../models/Tera/Pendaftaran Tera/KecamatanModel.js";

export const getSKHP = async (req, res) => {
    try {
        const skhp = await PenerbitanSKHP.findAll({
            include: [
                {
                    model: UTTP,
                    as: 'uttp',
                    include: [
                        {
                            model: PendaftaranTera,
                            as: 'pendaftaran_tera',
                            attributes: ["noOrder", "namaPemilik", "alamatLengkap"]
                        },
                        {
                            model: SatuanKapasitas,
                            as: 'satuan_kapasita',
                            attributes: ["nama"]
                        },
                        {
                            model: SubJenisUTTP,
                            as: 'sub_jenis_uttp',
                            attributes: ["jenis", "retribusi"]
                        }
                    ],
                    attributes: ["kapasitas", "merek", "model", "noSeri"],
                }
            ],
            attributes: ["status", "uttpId"]
        });
        res.json(skhp);
    } catch (error) {
        console.log(error);
    }
}

export const createSKHP = async (req, res) => {
    try {
        await PenerbitanSKHP.create({
            status: "BELUM DIVERIFIKASI",
            uttpId: req.body.id
        })
        res.json({ msg: "Pelayanan UTTP Berhasil" });
    } catch (error) {
        console.log(error);
    }
}

export const updateSKHP = async (req, res) => {
    try {
        const skhp = await PenerbitanSKHP.findOne({
            where: {
                uttpId: req.params.id
            }
        })
        skhp.status = req.body.aksi
        await skhp.save()
        res.json({ msg: "Update SKHP Berhasil" });
    } catch (error) {
        console.log(error);
    }
}