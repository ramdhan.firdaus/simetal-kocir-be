import Role from "../models/RoleModel.js"

export const getRole = async(req, res) => {
    try {
        const role = await Role.findAll();
        res.json(role);
    } catch (error) {
        console.log(error);
    }
}