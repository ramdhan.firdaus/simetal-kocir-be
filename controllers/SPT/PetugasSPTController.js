import PetugasSPT from "../../models/SPT/PetugasSPTModel.js";

export const getPetugasSPT = async (req, res) => {
    try {
        const petugas = await PetugasSPT.findAll({
            attributes: ["nip", "nama"]
        });
        res.json(petugas);
    } catch (error) {
        console.log(error);
    }
}

export const getPetugasSPTByNoOrder = async (req, res) => {
    try {
        const petugas = await PetugasSPT.findAll({
            where: { sptPendaftaranTeraNoOrder: req.params.no },
            attributes: ["nip", "nama"]
        });
        res.json(petugas);
    } catch (error) {
        console.log(error);
    }
}

export const createPetugasSPT = async (req, res) => {
    try {
        req.body.petugas.forEach((element) => {
            PetugasSPT.create({
                nip: element.nip,
                nama: element.nama,
                sptPendaftaranTeraNoOrder: req.params.no
            });
        });
        res.json({ msg: "Penambahan Petugas Berhasil" });
    } catch (error) {
        console.log(error);
    }
}

export const updatePetugasSPT = async (req, res) => {
    try {
        req.body.petugas.forEach(async (element) => {
            const petugas = await PetugasSPT.findOne({
                where: {
                    id: element.id
                }
            });
            petugas.nip = element.nip
            petugas.nama = element.nama
            await petugas.save()
        });
        res.json({ msg: "Update Petugas Berhasil" });
    } catch (error) {
        console.log(error);
    }
}

export const deletePetugasSPT = async (req, res) => {
    try {
        req.body.petugas.forEach(async element => {
            await PetugasSPT.destroy({
                where: {
                    id: element.id
                }
            });
        });
        res.json({ msg: "Delete Petugas Berhasil" });
    } catch (error) {
        console.log(error);
    }
}