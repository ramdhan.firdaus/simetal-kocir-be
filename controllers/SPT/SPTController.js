import SPT from "../../models/SPT/SPTModel.js"
import __dirname from "../../index.js"
import puppeteer from "puppeteer"

export const getSPT = async (req, res) => {
    try {
        const spt = await SPT.findAll({
            attributes: ["pendaftaranTeraNoOrder", "pemohon", "alamat",
                "tanggalPelaksanaan", "status"]
        });
        res.json(spt);
    } catch (error) {
        console.log(error);
    }
}

export const getSPTByNoOrder = async (req, res) => {
    try {
        const spt = await SPT.findOne({
            where: { pendaftaranTeraNoOrder: req.params.no },
            attributes: ["pemohon", "alamat",
                "tanggalPelaksanaan", "status", "updatedAt"]
        });
        res.json(spt);
    } catch (error) {
        console.log(error);
    }
}

export const createSPT = async (req, res) => {
    try {
        await SPT.create({
            pendaftaranTeraNoOrder: req.body.noOrder,
            pemohon: req.body.pemohon,
            tanggalPelaksanaan: req.body.tanggalPelaksanaan,
            alamat: req.body.alamat,
            status: "BELUM DIVERIFIKASI"
        });
        res.json({ msg: "Pendaftaran SPT Berhasil" });
    } catch (error) {
        console.log(error);
    }
}

export const updateAksiSPT = async (req, res) => {
    try {
        const spt = await SPT.findOne({
            where: {
                pendaftaranTeraNoOrder: req.params.no
            }
        });
        spt.status = req.body.aksi
        await spt.save()
        res.json({ msg: "Update SPT Berhasil" });
    } catch (error) {
        console.log(error);
    }
}

export const updateSPT = async (req, res) => {
    try {
        const spt = await SPT.findOne({
            where: {
                pendaftaranTeraNoOrder: req.params.no
            }
        })
        spt.pemohon = req.body.pemohon
        spt.tanggalPelaksanaan = req.body.tanggalPelaksanaan
        spt.alamat = req.body.alamat
        spt.save()
        res.json({ msg: "Update SPT Berhasil" });
    } catch (error) {
        console.log(error);
    }
}

export const createPDFSPT = async (req, res) => {
    const browser = await puppeteer.launch()
    const page = await browser.newPage()

    await page.goto(`http://localhost:3001/coba/${req.body.noOrder}`)
    await page.waitForTimeout(1000)
    await page.pdf({ path: `./uploads/pdf/spt-${req.body.noOrder}.pdf`, printBackground: true })

    console.log("Asdd")
    await browser.close()
    console.log("Asdd")
}

export const getPDFSPT = async (req, res) => {
    console.log(req.params.no)
    res.sendFile(`${__dirname}/uploads/pdf/spt-${req.params.no}.pdf`)
}