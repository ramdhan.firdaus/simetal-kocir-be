import Revisi from "../../models/SPT/RevisiSPTModel.js";

export const getRevisi = async (req, res) => {
    try {
        const revisi = await Revisi.findAll({
            attributes: ["detail", "namaPerevisi", "rolePerevisi", "status"]
        });
        res.json(revisi);
    } catch (error) {
        console.log(error);
    }
}

export const getRevisiByNoOrder = async (req, res) => {
    try {
        const revisi = await Revisi.findAll({
            where: {
                sptPendaftaranTeraNoOrder: req.params.no
            },
            attributes: ["id", "detail", "namaPerevisi", "rolePerevisi", "status"]
        });
        res.json(revisi);
    } catch (error) {
        console.log(error);
    }
}

export const createRevisi = async (req, res) => {
    try {
        await Revisi.create({
            sptPendaftaranTeraNoOrder: req.body.no,
            detail: req.body.detail,
            namaPerevisi: req.body.nama,
            rolePerevisi: req.body.role,
            status: "BELUM SELESAI"
        });
        res.json({ msg: "Revisi Berhasil" });
    } catch (error) {
        console.log(error);
    }
}

export const updateRevisi = async (req, res) => {
    try {
        const revisi = await Revisi.findOne({
            where: {
                id: req.params.id
            }
        });
        revisi.status = "SELESAI"
        revisi.save()
        res.json({ msg: "Revisi Berhasil" });
    } catch (error) {
        console.log(error);
    }
}