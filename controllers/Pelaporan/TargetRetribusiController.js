import TargetRetribusi from '../../models/Pelaporan/TargetRetribusiModel.js'

export const getTargetRetribusi = async (req, res) => {
    try {
        const target = await TargetRetribusi.findAll({
            attributes: ["tahun", "target"]
        });
        res.json(target);
    } catch (error) {
        console.log(error);
    }
}

export const getTargetRetribusiByTahun = async (req, res) => {
    try {
        const target = await TargetRetribusi.findOne({
            where: {
                tahun: req.params.tahun
            },
            attributes: ["tahun", "target"]
        });
        res.json(target);
    } catch (error) {
        console.log(error);
    }
}

export const createTargetRetribusi = async (req, res) => {
    try {
        await TargetRetribusi.create({
            tahun: req.body.tahun,
            target: req.body.target,
        });
        res.json({ msg: "Penambahan Target Berhasil" });
    } catch (error) {
        console.log(error);
    }
}