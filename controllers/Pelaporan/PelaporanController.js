import PelayananTera from "../../models/Tera/Pelayanan Tera/PelayananTeraModel.js";
import UTTP from "../../models/Tera/Pendaftaran Tera/UTTPModel.js";
import SubJenisUTTP from "../../models/Tera/Pendaftaran Tera/JenisUTTPSubModel.js";
import sequelize from "sequelize";
import PendaftaranTera from "../../models/Tera/Pendaftaran Tera/PendaftaranTeraModel.js";
import Kelurahan from "../../models/Tera/Pendaftaran Tera/KelurahanModel.js";
import Kecamatan from "../../models/Tera/Pendaftaran Tera/KecamatanModel.js";
import JenisUTTP from "../../models/Tera/Pendaftaran Tera/JenisUTTPModel.js";
import PenerbitanSKHP from "../../models/SKHP/SKHPModel.js"

export const getRekapitulasi = async (req, res) => {
    try {
        const tahun = parseInt(req.params.tahun)
        const pelaporan = await PelayananTera.findAll({
            where: {
                createdAt: {
                    [sequelize.Op.lt]: new Date(`${tahun + 1}`),
                    [sequelize.Op.gt]: new Date(`${tahun}`)
                }
            },
            include: [
                {
                    model: UTTP,
                    as: 'uttp',
                    include: [
                        {
                            model: SubJenisUTTP,
                            as: 'sub_jenis_uttp',
                            attributes: ["retribusi"]
                        }
                    ],
                    attributes: [],
                }
            ],
            attributes: ["statusPembayaran", "uttpId",
                [sequelize.fn('sum', sequelize.col('uttp.sub_jenis_uttp.retribusi')), 'total']],
            group: ['statusPembayaran'],
        });

        res.json(pelaporan);
    } catch (error) {
        console.log(error);
    }
}

export const getRekapitulasiByKecamatan = async (req, res) => {
    try {

        const kecamatan = await Kecamatan.findAll({
            attributes: ['id', 'nama']
        })

        const jenisUTTP = await JenisUTTP.findAll({
            attributes: ['id', 'jenis']
        })

        let data = {}
        kecamatan.map((element) => {
            data[element.nama] = {}
            jenisUTTP.map((el) => {
                data[element.nama][el.jenis] = 0
            })
            data[element.nama]['totalUTTP'] = 0
            data[element.nama]['totalSKHP'] = 0
            data[element.nama]['totalRetribusi'] = 0
        })

        res.json(data);
    } catch (error) {
        console.log(error);
    }
}

export const getRekapitulasiUTTPByKecamatan = async (req, res) => {
    try {
        const tahun = parseInt(req.params.tahun)
        const pelaporan = await UTTP.findAll({
            where: {
                createdAt: {
                    [sequelize.Op.lt]: new Date(`${tahun + 1}`),
                    [sequelize.Op.gt]: new Date(`${tahun}`)
                }
            },
            include: [
                {
                    model: SubJenisUTTP,
                    as: 'sub_jenis_uttp',
                    attributes: ['jenisUttpId'],
                    include: [
                        {
                            model: JenisUTTP,
                            as: 'jenis_uttp',
                            attributes: ['jenis']
                        }
                    ],
                },
                {
                    model: PendaftaranTera,
                    as: 'pendaftaran_tera',
                    attributes: ['kelurahanId'],
                    include: [
                        {
                            model: Kelurahan,
                            as: 'kelurahan',
                            attributes: ['kecamatanId'],
                            include: [
                                {
                                    model: Kecamatan,
                                    as: 'kecamatan',
                                    attributes: ['nama']
                                }
                            ],
                        }
                    ],
                }
            ],
            attributes: ['id',
                [sequelize.fn('COUNT', sequelize.col('sub_jenis_uttp.jenisUttpId', 'pendaftaran_tera.kelurahan.kecamatanId')), 'count']],
            group: ['sub_jenis_uttp.jenisUttpId', 'pendaftaran_tera.kelurahan.kecamatanId']
        });

        res.json(pelaporan);
    } catch (error) {
        console.log(error);
    }
}

export const getRekapitulasiSKHPByKecamatan = async (req, res) => {
    try {
        const tahun = parseInt(req.params.tahun)
        const skhp = await PenerbitanSKHP.findAll({
            where: {
                createdAt: {
                    [sequelize.Op.lt]: new Date(`${tahun + 1}`),
                    [sequelize.Op.gt]: new Date(`${tahun}`)
                }
            },
            include: [
                {
                    model: UTTP,
                    as: 'uttp',
                    include: [
                        {
                            model: PendaftaranTera,
                            as: 'pendaftaran_tera',
                            attributes: ['kelurahanId'],
                            include: [
                                {
                                    model: Kelurahan,
                                    as: 'kelurahan',
                                    attributes: ['kecamatanId'],
                                    include: [
                                        {
                                            model: Kecamatan,
                                            as: 'kecamatan',
                                            attributes: ['nama']
                                        }
                                    ],
                                }
                            ],
                        }
                    ],
                    attributes: ['id'],
                }
            ],
            attributes: [[sequelize.fn('COUNT', sequelize.col('uttp.pendaftaran_tera.kelurahan.kecamatanId')), 'count']],
            group: ['uttp.pendaftaran_tera.kelurahan.kecamatanId']
        });

        res.json(skhp);
    } catch (error) {
        console.log(error);
    }
}

export const getRekapitulasiRetribusiByKecamatan = async (req, res) => {
    try {
        const tahun = parseInt(req.params.tahun)
        const skhp = await UTTP.findAll({
            where: {
                createdAt: {
                    [sequelize.Op.lt]: new Date(`${tahun + 1}`),
                    [sequelize.Op.gt]: new Date(`${tahun}`)
                }
            },
            include: [
                {
                    model: SubJenisUTTP,
                    as: 'sub_jenis_uttp',
                    attributes: ['jenisUttpId'],
                },
                {
                    model: PendaftaranTera,
                    as: 'pendaftaran_tera',
                    attributes: ['kelurahanId'],
                    include: [
                        {
                            model: Kelurahan,
                            as: 'kelurahan',
                            attributes: ['kecamatanId'],
                            include: [
                                {
                                    model: Kecamatan,
                                    as: 'kecamatan',
                                    attributes: ['nama']
                                }
                            ],
                        }
                    ],
                },
            ],
            attributes: [[sequelize.fn('SUM', sequelize.col('sub_jenis_uttp.retribusi')), 'sum']],
            group: ['pendaftaran_tera.kelurahan.kecamatanId']
        });

        res.json(skhp);
    } catch (error) {
        console.log(error);
    }
}